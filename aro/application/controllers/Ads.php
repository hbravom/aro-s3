<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/*! Clase de Ads */
class Ads extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		
		$this->load->helper('url','html');
		$this->load->library('grocery_CRUD');
		$this->load->library('s3');

		$this->load->database();

		
		if($this->session->userdata('logged_in'))
		{

		}else{
			redirect('login', 'refresh');	
		}

	}

	/**
	 *  Metodo Output()
	 *  
	 *  Este método retorna una vista para los diferentes CRUDS de grocery
	 */	
	public function Output($output = null)
	{


			//recibimos la info de session del usuario
		$session_data = $this->session->userdata('logged_in');
		$data['session_data'] = $session_data;
				//view		


		$this->load->view('templates/header',$data);
		$this->load->view('ads.php',$output);
		$this->load->view('templates/footer');


	}

	
	/**
	 *  Metodo index()
	 *  
	 *  Este método index() agrega css, jss a la vista principal
	 */		
	public function index()
	{	
		$this->Output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}

	/**
	 *  Metodo prerolls()
	 *  
	 *  CRUP para la tabla de Prerolls
	 */		
	public function prerolls()
	{

		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('preroll');
			$crud->set_subject('Preroll ');
			//$crud->required_fields('preroll');
			//$crud->columns('city','country','phone','addressLine1','postalCode');
			$crud->set_relation('radio','radios','nombre');
			$crud->set_relation('estado','estados','nombre');
			$crud->set_relation('tipo','tipos','nombre');

			$crud->set_field_upload('file','assets/uploads/files');
			
			$crud->callback_after_insert(array($this, 'update_json_prerolls')); //insert
			$crud->callback_after_update(array($this, 'update_json_prerolls')); //update
			$crud->callback_after_delete(array($this,'update_json_prerolls'));	//delete

			$output = $crud->render();

			$this->Output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
				
	}

	function update_json_prerolls($post_array,$primary_key)
	{
		//verificar que tipo está activo

		
		$this->update_html($primary_key);

		$query = $this->db->query('SELECT * from  preroll');
		

		if ($query->num_rows() > 0) {
			
			$fp = fopen(getcwd().'/assets/prerolls.json', 'w');
			fwrite($fp, json_encode($query->result(), JSON_FORCE_OBJECT));
			fclose($fp);
			return true;
		}else{

			return false; 
		}	
		
	}

	/**
	 *  Metodo enrutadores()
	 *  
	 *  CRUP para la tabla de enrutadores
	 */
	public function enrutadores()
	{

		
		if( $this->session->userdata['logged_in']['rol']!='Admin')	{	
     				//If no session, redirect to login page
			redirect('home', 'refresh');
		}	
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('enrutador');
			$crud->set_subject('Enrutadores ');
			//$crud->required_fields('preroll');
			//$crud->columns('city','country','phone','addressLine1','postalCode');
			$crud->set_relation('radio','radios','nombre');
			$crud->set_relation('estado','estados','nombre');


			$output = $crud->render();

			$this->Output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
		




		
	}

	


	/**
	 *  Metodo radios()
	 *  
	 *  CRUP para la tabla de radios
	 */
	public function radios()
	{


		
		if( $this->session->userdata['logged_in']['rol']!='Admin')	{	
     				//If no session, redirect to login page
			redirect('home', 'refresh');
		}

		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('radios');
			$crud->set_subject('Radios ');

			$output = $crud->render();

			$this->Output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
		




		
	}

	public function estados()
	{



		if( $this->session->userdata['logged_in']['rol']!='Admin')	{	
     				//If no session, redirect to login page
			redirect('home', 'refresh');
		}

		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('estados');
			$crud->set_subject('Estados ');

			$output = $crud->render();

			$this->Output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}


	/**
	 *  Metodo usuarios()
	 *  
	 *  CRUP para la tabla de usuarios
	 */
	public function usuarios()
	{

		if( $this->session->userdata['logged_in']['rol']!='Admin')	{	
     				//If no session, redirect to login page
			redirect('home', 'refresh');
		}

		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('users');
			$crud->set_subject('Usuarios ');
			$crud->set_relation('estado','estados','nombre');

			$output = $crud->render();

			$this->Output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

/**
	 *  Metodo tipos()
	 *  
	 *  CRUP para la tabla de tipos
	 */
public function tipos()
	{

		if( $this->session->userdata['logged_in']['rol']!='Admin')	{	
     				//If no session, redirect to login page
			redirect('home', 'refresh');
		}

		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('tipos');
			$crud->set_subject('Tipos file ');
			$crud->set_relation('estado','estados','nombre');

			$output = $crud->render();

			$this->Output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function update_html($primary_key)
	{
		$this->load->model('Code_model');

		$query = $this->db->get_where('preroll', array('id' => $primary_key));
        $elemento = $query->row();

        $tipo = $elemento->tipo;
        $estado = $elemento->estado;
        $radio = $elemento->radio;
        $file = $elemento->file;

        if($estado == 1)
        {
	        // Validar Si el preroll está activo
	        $query_enr = $this->db->get_where('enrutador', array('radio' => $radio));
	        $enrutador = $query_enr->row();
	        $opcion = $enrutador->opcion;

	        switch ($opcion) {
	        	case 1:
	        		$html_mobile = $this->Code_model->saveHtmlNinguno($radio,1);	
					$html_desktop = $this->Code_model->saveHtmlNinguno($radio,0);
	        		break;

	        	case 2:
	        		$html_mobile = $this->Code_model->saveHtmlAro($radio, $file, $tipo, 1);	
					$html_desktop = $this->Code_model->saveHtmlAro($radio, $file, $tipo, 0);	
	        		break;
	        	
	        	case 3:
	        		$html_mobile = $this->Code_model->saveHtmlTap($radio,1);	
					$html_desktop = $this->Code_model->saveHtmlTap($radio,0);
	        		break;
	        }

	        $this->Code_model->saveHTML($radio, 'mobile', $html_mobile);
        	$this->Code_model->saveHTML($radio, 'desktop', $html_desktop);

 		}
       
	}




	
}