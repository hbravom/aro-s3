<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->database();
	
	}


	


	function index()
	{
		if($this->session->userdata('logged_in'))
		{
				//recibimos la info de session del usuario
			$session_data = $this->session->userdata('logged_in');
			$data['session_data'] = $session_data;
				//view		

			$this->load->view('templates/header', $data);
			$this->load->view('home/index', $data);
			$this->load->view('templates/footer');

		}
		else
		{
     				//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}

	/**
	 * 
	 * 
	 * El método logout(), cierra la sessión activa del usuario.
    *	 	 
	 */
	function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('home', 'refresh');
	}


   /**
	 * 
	 * El método cachecontrol(), cierra la sessión activa del usuario.
    *	 	 
    *	 	 
	 */
	public function cachecontrol()
	{
			
		if($this->session->userdata('logged_in'))
		{
				//recibimos la info de session del usuario
			$session_data = $this->session->userdata('logged_in');
			$data['session_data'] = $session_data;
				//view		

			$this->load->view('templates/header', $data);
			$this->load->view('services/cachecontrol', $data);
			$this->load->view('templates/footer');

		}
		else
		{
     			
			redirect('login', 'refresh');
		}
			

		
	}





}
