<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*! Clase  Login */
class Login extends CI_Controller {
 
 
 function __construct()
 {
   parent::__construct();
   
   $this->load->helper('security');
   $this->load->helper('url');

   $this->load->library('session');


  		 if($this->session->userdata('logged_in'))
		{
			redirect('home', 'refresh');	

		}

 }

 /**
  * Método index() 
  *  
  *  Devuelve la vista principal de login
  * 
  */

 function index()
 {
 
     $this->load->helper(array('form'));

     $this->load->view('login/index');
 

 }
 
}
 
?>