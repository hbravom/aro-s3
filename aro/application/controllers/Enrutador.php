<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*! Clase Enrutador */
class Enrutador extends CI_Controller {

	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->helper('url');
		$this->load->library('session');
		
		$this->load->database();
	}


	public function index()
	{
		if($this->session->userdata('logged_in'))
		{
				//recibimos la info de session del usuario
			$session_data = $this->session->userdata('logged_in');
			$data['session_data'] = $session_data;
				//view					
			$this->load->view('templates/header', $data);
			$this->load->view('enrutador/index', $data);
			$this->load->view('templates/footer');

		}
		else
		{
     				//If no session, redirect to login page
			redirect('login', 'refresh');
		}		
		
	}

	/**
       * El método update(), actualiza el enrutador general  y actulizanfo en la tabla por el id de radio.
	    *  
	  	 * Una vez actualizado la tabla , este funcion lee el contenido y genera el nuevo enrutador.json
	  	 * 
       * @param id (Identificador de radio)
       * @param opcion ( Ninguno / Aro / Tap )
	    *   
	  *   
	  */

	public function update($id,$opcion)
	{
		$this->load->model('Code_model');

			$id=$id +1;
			$data_ = array(
			'opcion' => $opcion
			);

		$this->db->where('id', $id);
		$this->db->update('enrutador', $data_); //Actulizando el enrutador

		//create html
		$this->Code_model->createAllHTML();

		$query = $this->db->query('SELECT  r.nombre as radio, e.opcion FROM enrutador e , radios r where e.radio=r.id and  e.estado=1');	
		if ($query->num_rows() > 0) {			
			$fp = fopen(getcwd().'/assets/enrutador.json', 'w'); //actualizando el nuevo enrutador.json
			fwrite($fp, json_encode($query->result(), JSON_FORCE_OBJECT));
			fclose($fp);
			redirect('/enrutador/', 'refresh');
		}else{

			echo "No hay datos que mostrar.";
		}

	}





}
