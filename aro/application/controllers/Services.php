<?php
header('Access-Control-Allow-Origin: *');
//header('Access-Control-Allow-Methods: GET, POST');  
defined('BASEPATH') OR exit('No direct script access allowed');

/*! Clase de Servicios */
class Services extends CI_Controller {

	
	public function __construct()
	{
		parent::__construct();		
		$this->load->helper('url');	

	}

	/**
	 *  Metodo index()
	 *  
	 */
	public function index()
	{
		$this->load->view('templates/header', $data);
		$this->load->view('services/index', $data);
		$this->load->view('templates/footer');

	}
	
	

	  /**
       * El método ws_ARO, devuelve los datos del preroll  registrada
       * @param radio (Identificador)
       * @param dispositivo (Mobile, Desktop)
	    *   
       * @return $data['preroll'] , este array contiene la info del Preroll
       */

	public function ws_ARO($radio,$dispositivo)
	{	

		$rand=rand(2,500);
		$string = file_get_contents(base_url()."assets/enrutador.json?val=".$rand);  //Leemos la data del enrutador.json

		$enrutador = json_decode($string, true); /*!< string value enrutador. */  
		$opcion=0;

		for ($i=0; $i< count($enrutador); $i++){

			if($radio== $enrutador[$i]['radio']){
				$opcion=$enrutador[$i]['opcion'];
			}
		}
	
		$data['dispositivo']=$dispositivo; 
		$data['radio']=$radio;  //guarda el Id de la radio
		
		
		switch ($opcion) {
			case 1:  //CASO NINGUNO si la radio no tiene preroll audio o video se ejecuta directamente el Streaming
			$this->load->view('services/ninguno',$data);
			break;

			case 2:	//CASO ARO es el caso principal. Si la radio tiene un audio o video preroll activo, se procede a leer los datos del preroll y guardarlos en session

			//Get Info Radio			
			//Leer radios File Json 
			$string = file_get_contents(base_url()."assets/radios.json?val=".$rand);
			$radios = json_decode($string, true);
			$radio_id=0;
			

			for ($i=1; $i<= count($radios); $i++){

				if($radio== $radios[$i]['radio']){
				   $radio_id=$radios[$i]['id'];
				}
			}
			$data['radio_id']=$radio_id;			

			//Leer Prerroll File Json 
			$json_prerolls = file_get_contents(base_url()."assets/prerolls.json");
			$prerolls_data = json_decode($json_prerolls, true);
			
			
			/* Si no tenemos resultado la salida será como ninguno */
			if(count($prerolls_data) <= 0){
				
				$this->load->view('services/ninguno',$data);
				

			}else{
				$file_name="";

				for ($i=0; $i< count($prerolls_data); $i++){				

					//Solo obtenemos el registro de la Radio indicada y el Estado activo
					if($radio_id == $prerolls_data[$i]['radio'] ){				 
					
						$data['preroll']['nombre']= $prerolls_data[$i]['nombre'];
						$data['preroll']['tipo'] = $prerolls_data[$i]['tipo'];
						$data['preroll']['duracion']= $prerolls_data[$i]['duracion'];
						$data['preroll']['estado']= $prerolls_data[$i]['estado'];						

						$file_name=$prerolls_data[$i]['file']; //quitndo los primero 6 caracteres que agrega CI al nombre del archivo
						$data['preroll']['file']=$file_name;

					}
				}
				
				
				//BUCKET INFO
				$BUCKET='http://ads.crp.com.pe.s3.amazonaws.com/';
				$CARPETA_PREROLL='prerolls/';

				//Ruta final del File
				$PATH_FILE=$BUCKET.$CARPETA_PREROLL.$file_name;

				$data['preroll']['path_file'] = $PATH_FILE;

				$this->load->view('services/aro',$data);

			}


		break;

		case 3: //CASO TAP se ejecuta el Streaming con la llamada PlayTap();
		$this->load->view('services/tap',$data);

		break;

		default:
		$this->load->view('services/ninguno',$data);
		break;
	}	

}
}
