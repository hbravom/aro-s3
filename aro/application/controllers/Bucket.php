<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*! Clase de bucket */
class bucket extends CI_Controller {


	public function __construct()
	{
		parent::__construct();	
		$this->load->helper('url');
		 // Load Library
  		$this->load->library('s3');  

		$this->load->library('session');
	 
	}

	/**
	 *  Metodo index()
	 *  
	 *  Este método retorna la vista de los archivos subidos al Bucket S3
	 */	
	public function index()
	{

		if($this->session->userdata('logged_in'))
		{
				//recibimos la info de session del usuario
			$session_data = $this->session->userdata('logged_in');
			$data['session_data'] = $session_data;
				//view					
			$this->load->view('templates/header', $data);
			$this->load->view('bucket/index', $data);
			$this->load->view('templates/footer');

		}
		else
		{
			redirect('login', 'refresh');
		}			
	
	


	}
}
