<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
/*! Clase  VerifyLogin */
class VerifyLogin extends CI_Controller {
 
 function __construct()
 {
   parent::__construct();

   $this->load->library('session');
   $this->load->database();
   $this->load->model('user','',TRUE);
   
 }
 
 /**
       * El método index(), valida el Login y el Proceso de Logeo
     
       * @return vista del index
       */

 function index()
 {
   //This method will have the credentials validation
   $this->load->library('form_validation');
 
   $this->form_validation->set_rules('username', 'Username', 'trim|required');
   $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_check_database');
 
   if($this->form_validation->run() == FALSE)
   {
     //Field validation failed.  User redirected to login page
     $this->load->view('login/index.php');
   }
   else
   {
     //Go to private area
     redirect('home', 'refresh');
   }
 
 }
 
 /**
       * El método check_database, valida el inicio de session
       * @param password 
     
       * @return $data['preroll'] , este array contiene la info del Preroll
       */
 function check_database($password)
 {
   //Si el campo de username fue enviado satisfactoriamente
   $username = $this->input->post('username');
 
   //query a la tabla user
   $result = $this->user->login($username, $password);
 
   if($result)
   {
     $sess_array = array();
     foreach($result as $row)
     {
       $sess_array = array(
         'id' => $row->id,
         'username' => $row->username,
         'nombres' => $row->nombres,
         'apellidos' => $row->apellidos,
         'rol' => $row->rol

       );
       $this->session->set_userdata('logged_in', $sess_array); //guardamos la session del usuario
     }
     return TRUE;
   }
   else
   {
     $this->form_validation->set_message('check_database', 'Invalid username or password');
     return false;
   }
 }
}
?>