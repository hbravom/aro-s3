<?php
/*! Clase de User Model */
Class Code_model extends CI_Model
{

  	private $CI;

	  public function __construct()
	  {
	  	parent::__construct();
	   	$this->CI =&get_instance(); 
	   	$this->load->library('s3');
	  }


	 function saveHtmlAro($radio, $path, $tipo, $dispositivo)
	 {
	 	$path = 'https://s3.amazonaws.com/ads.crp.com.pe/prerolls/'.$path;
	 	//$path = 'http://ads.crp.com.pe.s3.amazonaws.com/prerolls/'.$path;
	
	 	$html = '';
		switch ($tipo) 
		{
		   	case 1:

		   		if($dispositivo == 'desktop') //DESKTOP
				{
					$html = '

		   				<audio autoplay id="AudioPreroll" >
					        <source src="'.$path.'" type="audio/mp3">
					        Tu Browser no soporta Html5 Video. <a href="" onclick=" VideoEnd();"> Continuar</a>
					    </audio>
					    <script>
					    	
					        pre_roll=1;			       
					        var AudioPreroll = document.getElementById("AudioPreroll");
							
							 AudioPreroll.addEventListener("play", function () {
								ga(\'send\', \'event\', \' tracking Streaming\',\'Play Preroll\');
							 });

			        		AudioPreroll.addEventListener("ended", function () {	     		
				       	        ga(\'send\', \'event\', \' tracking Streaming\',\'End Preroll\');
			        			console.log(\'Evento Analitycs Ok\');

								ReadyStreaming();
								ShowBtnPause();			  

								$("#pauseButton" ).attr( "onclick", "  ShowBtnPlay(); pauseStream();");
				      			$("#playButton" ).attr( "onclick", "ShowBtnPause(); playStream();" );		
				      		

								flag_escucho=true;
			        			console.log("Se escuchó por primera vez: var flag_escucho ="+ flag_escucho);
					       	});

					        AudioPreroll.addEventListener("error", function () {
				            	console.log("Preroll: error de carga de cuña");
				            	ended = 1; //variable indica video acabó
				            	ReadyStreaming();  //iniciar el Streaming con Segid

			        		});
			        		</script>';

				}
				else
				{
					$html = '
						<audio autoplay id="AudioPreroll" >
					        <source src="'.$path.'" type="audio/mp3">
					        Tu Browser no soporta Html5 Video. <a href="" onclick=" VideoEnd();"> Continuar</a>
					    </audio>

					    <script>
					    	
					        pre_roll=1;			       
					        var AudioPreroll = document.getElementById("AudioPreroll");
							
							AudioPreroll.play();
							AudioPreroll.addEventListener("play", function () {
								ga(\'send\', \'event\', \' tracking Streaming\',\'Play Preroll\');
							});

			        		AudioPreroll.addEventListener("ended", function () {	     		
				       	        ga(\'send\', \'event\', \' tracking Streaming\',\'End Preroll\');
			        			console.log(\'Evento Analitycs Ok\');

							    $( "#pauseButton" ).attr( "onclick", "ShowBtnPlayMobile(); pauseStream();");           
						        $( "#playMobile" ).attr( "onclick", "ShowBtnPauseMobile(); playStream();" );
							    
					        	flag_escucho=true;
			        			console.log("Se escuchó por primera vez: var flag_escucho ="+ flag_escucho);
					       	});

					        AudioPreroll.addEventListener("error", function () {
				            	console.log("Preroll: error de carga de cuña");
				            	ended = 1; //variable indica video acabó
				            	ReadyStreaming();  //iniciar el Streaming con Segid
			        		});

					     
							ShowBtnPlayMobile();
							$("#modalAro").modal("show");
							$("#playMobile").attr( "onclick", "AudioPreroll.play();" );   	
		    		    </script>';
				}
		   		

					    

					    
		   		break;
		   	
		   	case 2:
		   		$html = '
		   			<script>
		   				console.log("ARO"+'.$radio.');	
						var video = $("<video />", {
							id: "videopreroll",
							src: "'.$path.'",
							type: "video/mp4",
							controls: true,
							autoplay: true,
							height: 350
						});
						video.appendTo($("#div_video"));
						var aud = document.getElementById("videopreroll");';

						if($dispositivo == 'mobile')
						{	
							$html .= 'aud.controls = true;
							aud.width = 320; 
							aud.play();';
						}
						
						
				$html .= ' aud.addEventListener("play", function () {
							ga(\'send\', \'event\', \' tracking Streaming\',\'Play Video Preroll\');
							console.log("Video Preroll - Play");
						 });

					    aud.addEventListener("ended", function () {	     		
							ga(\'send\', \'event\', \' tracking Streaming\',\'End Video Preroll\');
							console.log(\'Evento Analitycs Ok\');
							console.log("Video Preroll - End");
						 });


						aud.onended = function() {
						  
						    $("#div_video").hide();';

					    		if($dispositivo == 'desktop') //DESKTOP
							    {
							    	$html .= 'ReadyStreaming();
							    	ShowBtnPause();						          
							      
							      	$( "#pauseButton" ).attr( "onclick", "  ShowBtnPlay(); pauseStream();" );
		      				   		$( "#playButton" ).attr( "onclick", "ShowBtnPause(); playStream();" );';   				                     
							    	
							    }
							    else  if($dispositivo == 'mobile') //MOBILE
							    {						    	  

							    	$html .= 'ShowBtnPlayMobile();
							            $( "#pauseButton" ).attr( "onclick", "ShowBtnPlayMobile(); pauseStream();");           
							            $( "#playMobile" ).attr( "onclick", "ShowBtnPauseMobile(); playStream();" ); ';					         
							    }    
						 
						$html .= 'ended = 1;
						};
						</script>';
		   		break;

		   		
		}

	   return $html;
	 }

	 function saveHTML($radio, $carpeta, $html)
	 {
	 	 $CI =& get_instance();
		 //Invocamos la libreria
		 $CI->load->library('s3');

	 	$bucket = 'ads.crp.com.pe';
	 	$file = $radio.".htm";
	 	$ruta = $bucket."/prerolls/html/".$file;

	 	// Crear HTML
	 	file_put_contents("html/".$carpeta."/".$radio.'.htm', $html);
	 	chmod("html/".$carpeta."/".$radio.'.htm', 0755);  

	 	// S3
	 	$CI->s3->putObjectFile("html/".$carpeta."/".$file, $bucket,'prerolls/html/'.$carpeta.'/'.$file,'public-read');
	 }

	 function saveHtmlNinguno($radio, $dispositivo)
	 {
	 	$html = '<script>';

					if($dispositivo == '0') 
					{
					$html .= '
					   console.log("NINGUNO"+'.$radio.');	
					   playStreamOnly();				
					   ShowBtnPause();						          
					   $("#pauseButton").attr( "onclick", "ShowBtnPlay(); pauseStream();" );
	  				   $("#playButton").attr( "onclick", "ShowBtnPause(); playStreamOnly();" );';
	  				             
					} 
					else if ($dispositivo == '1') 
					{	
					$html .= '
					   console.log("NINGUNO"+'.$radio.');	
					   ShowBtnPlayMobile();
					   $("#modalAro").modal("show");	
					   $("#pauseButton").attr("onclick", "ShowBtnPlayMobile(); pauseStream();");           
					   $("#playMobile").attr("onclick", "ShowBtnPauseMobile(); playStreamOnly();");';
					}					
		$html .= '</script>';

		return $html;
	 }

	function saveHtmlTap($radio, $dispositivo)
	 {
	 	$html = '<script>';

					if($dispositivo == '0') 
					{
					$html .= '	
						console.log("TAP"+'.$radio.');	
					  	playTAPAd();
						ShowBtnPause();						          
						$( "#pauseButton" ).attr( "onclick", "  ShowBtnPlay(); pauseStream();");
						$( "#playButton" ).attr( "onclick", "ShowBtnPause(); playStream();" );
						flag_escucho=false;';
	  				             
					} 
					else if ($dispositivo == '1') 
					{	
					$html .= '
						console.log("TAP"+'.$radio.');	
					   	$("#modalAro").modal("show");	
						ShowBtnPlayMobile();
						$( "#pauseButton" ).attr( "onclick", "ShowBtnPlayMobile(); pauseStream();" );           
						$( "#playMobile" ).attr( "onclick", "ShowBtnPauseMobile(); playTAPAdMobile();" );';
					}					
		$html .= '</script>';

		return $html;
	 }

	function createAllHTML()
	{
		$query = $this->db->get_where('enrutador', array('estado' => '1'));

        if($query->num_rows() > 0)
		{
			$enrutadores = $query->result();

			foreach($enrutadores as $enrutador)
			{
				$html_mobile = '';
				$html_desktop = '';
				$radio = $enrutador->radio;
				$opcion = $enrutador->opcion;
				$estado = $enrutador->estado;

				if($estado == 1)
		        {
			        switch ($opcion) {
			        	case 1:
			        		$html_mobile = $this->saveHtmlNinguno($radio, 1);	
							$html_desktop = $this->saveHtmlNinguno($radio, 0);
			        		break;

			        	case 2:

			        		$query2 = $this->db->get_where('preroll', array('radio' => $radio, 'estado' => '1'));
        					$elemento = $query2->row();
        				
        					if(!empty($elemento))
        					{
        						$tipo = $elemento->tipo;
						        $file = $elemento->file;

				        		$html_mobile = $this->saveHtmlAro($radio, $file, $tipo, 'mobile');	
								$html_desktop = $this->saveHtmlAro($radio, $file, $tipo, 'desktop');

        					}			        			
			        		break;
			        	
			        	case 3:
			        		$html_mobile = $this->saveHtmlTap($radio,1);	
							$html_desktop = $this->saveHtmlTap($radio,0);
			        		break;
			        }

			        $this->saveHTML($radio, 'mobile', $html_mobile);
		        	$this->saveHTML($radio, 'desktop', $html_desktop);
		 		}
				

			}		
		}
		return TRUE;
	
	}







}
?>