/**
VERSION: BETA-2.9
fecha: 29 /05 /2017
CRP Medios Y Entretenimiento
**/

// AUTOPLAY
//1= mobile; 0= desktop
var mobile = ismobile();
var autoPlay = false ; //tipo de inicio del player
var flag_mobile = false;

function ismobile(){
  if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    return 1;
  }else
  {
    return 0;
  }
}

function Itunes() {
  var photo_src = "/assets/images/logo-radiomar.png";
  var photo = 0;
  $(function () {
    $.ajax({
      url: "http://itunes.apple.com/search?term=" + titulo + "&limit=1",
      dataType: 'jsonp',
      success: function (json_results) {
        // console.log(json_results);
        $.each(json_results.results, function (key) {
          // edit
          var data = json_results.results[key],
          artworkUrl100 = ReplaceSizePhoto(data.artworkUrl100) || '',
          collectionName = data.collectionName || '';
          primaryGenreName = data.primaryGenreName || '';

          photo = 1;
          $("#itunes_image").attr("src", artworkUrl100);
        });

        //Si no hay result del Api
        if (photo == 0) {
          $("#itunes_image").attr("src", photo_src);
        }
      }
    });
  })
}
//FUNCION PARA REDIMENZIONAR LA IMAGEN DE ITUNES
function ReplaceSizePhoto(url) {
  var str = url;
  var res = str.replace(/100x100bb/g, "200x200bb");
  return res;
}


//VOLUME BAR
//volume bar event
var volumeDrag = false;
$('#muteButton').on('click', function(e){
  player.mute();
  updateVolume(e.pageX);
});

$('#unMuteButton').on('click', function(e){
  updateVolume(100,1);
});
$('.barr-volum').on('mousedown', function (e) {
    volumeDrag = true;
    player.unMute();
    //$('.sound').removeClass('muted');
    updateVolume(e.pageX);
});
$(document).on('mouseup', function (e) {
  if (volumeDrag) {
    volumeDrag = false;
    updateVolume(e.pageX);
  }
});
$(document).on('mousemove', function (e) {
  if (volumeDrag) {
    updateVolume(e.pageX);
  }
});
var updateVolume = function (x, vol) {
  var volume = $('.barr-volum');
  var percentage;
  //if only volume have specificed
  //then direct update volume
  if (vol) {
      percentage = vol * 100;
  } else {
    var position = x - volume.offset().left;
    percentage = 100 * position / volume.width();
  }
  if (percentage > 100) {
    percentage = 100;
  }
  if (percentage < 0) {
    percentage = 0;
  }
  //update volume bar and video volume
  $('.volumenBar').css('width', percentage + '%');
  var secvol = (percentage / 100);
  player.setVolume(secvol);
  //change sound icon based on volume
  if (secvol == 0) {
    $('.volume-icon').addClass('vol-off');
    $('.volume-icon').removeClass('vol-hard vol-med');
  } 
  else if (secvol >= 0.5) {
    $('.volume-icon').addClass('vol-hard');
    $('.volume-icon').removeClass('vol-off vol-med');
  } 
  else {
    $('.volume-icon').addClass('vol-med');
    $('.volume-icon').removeClass('vol-off vol-hard');      
  }
};


function comscore(param){
  if (typeof udm_ == 'function') {
    var name = (typeof cname !== "undefined")?cname:"";
    if (name){
      udm_('http' + (document.location.href.charAt(4) == 's' ? 's://sb' : '://b') + '.scorecardresearch.com/p?c1=2&c2=6906600&ns_site=mar-radio&name=' + name);
    }
  }
  if (typeof ga == 'function') {
    ga('send', 'pageview', window.location.pathname + '#' + param);
  } else if (typeof _gat == 'object') {
    _gat._getTracker('UA-20381111-1')._trackPageview();
  }
}
// ---------------------------PRE-ROlL Video ---------------------------------------//
function ServiceARO(radio,mobile) {
  var dispositivo='desktop';
  if(mobile==1){
    dispositivo='mobile'
  }
  
  jQuery.support.cors = true;

  $.ajax({
    //url: 'http://concursos.crp.pe/security/management_preroll/apiprerolls/mobile/radio:' + radio,
    //url: 'http://concursos.crp.pe/security/ARO/services/ws_ARO/'+radio+'/'+dispositivo,
    url: 'http://concursos.crp.pe/security/ARO/Services/ws_ARO/'+radio+'/'+dispositivo,
    type: 'get',
    cache: false,
    dataType: "html",
    beforeSend: function () {
      $("#action-ajax-result").html("");
      // console.log("Antes de enviar limpiar los espacios de publicidad");
    },
    success: function (response) {
      $("#action-ajax-result").html(response);
      console.log(" Respuesta del Service ARO : OK");
    },
    error: function (xhr) {
      //default
      console.log("Respuesta del Service ARO : ERRO");
      pre_roll = 0; //No hay pre-roll // Tdplayer lee variable -> play Streaming
      ended = 1; //variable indica video acabÃ³
    }
  });
}







/**SOCIAL SHARED MODAL**/
$("#button_shared_modal").click( function()
  {
    $(".bg_social_shared").toggleClass('social_active');
  }
);
$(".title_ss, .bg_social_shared").click( function()
  {
    $(".bg_social_shared").removeClass('social_active');
  }
);
/**FIN SOCIAL SHARED MODAL**/
/**NUEVO MENU**/

function showSubMenu(btn)
{
  $(btn).parent("li").toggleClass("show-prueba");
}
/**/


$('#btn_search2').click(function(){
      
    $("#inn-push").toggleClass("search-show-fixed");
    $("#input-search2").toggleClass("show-input");
    $(".close-fixed-search").toggleClass("show-close");
    $("#btn_search2").toggleClass("fa-times");
});




/*funciones mavegacion mobile y navegación fixed*/

$('#nav-icon1').click(function(){
  $(this).toggleClass('open-ham');
  $(".nav-fixed").toggleClass("open-nav-fixed");
});    


$('.close-gen-mob').click(function(){
    closeNav();
    $(".acordeonNav").removeClass("show-prueba"); 
});


  

$( ".more_navmobile" ).click(function() {
  $(this).children().toggleClass('fa-angle-up fa-angle-down');
  $(this).next().toggleClass('menu_desplegado_mobile');
});
$( ".open_navmobile" ).click(function() {
  $(".wrapper_navmobile").toggleClass('show_navmobile');
  $("body, html").css("overflow-y", "hidden");
});
$(".close_navmobile, .overlay_nav").click(function(){
  $(".wrapper_navmobile").removeClass("show_navmobile");
  $("body, html").css("overflow-y", "auto");
});
/**NUEVO MENU**/

///SCROLL MENU FIXED///
  var altura_nav = $("#wrapper-menu").offset().top + 40;
  $(window).scroll(function(event){
    if ($(this).scrollTop() > altura_nav){
      $("#menu-fixed").addClass('menu-fixed-activado');
      $("#menu-principal").addClass('menu-fixed-secundario');
    }
    else {
      $("#menu-fixed").removeClass('menu-fixed-activado');
      $("#menu-principal").removeClass('menu-fixed-secundario');
    }
  });
  

  if ($(window).width() > 980) {
    $(".menu_activo").hover( function()
      {
        $("#menu-principal").addClass('menu_more_activo');
      }
    );

    $(document).on('mouseleave', '#wrapper-menu',function(){
      $("#menu-principal").removeClass('menu_more_activo');
    });
  }

  $('#trigger-overlay, .overlay-close, .menu_activo').click(function(){
    $('.overlay').toggleClass('open');
  });