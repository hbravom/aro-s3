<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Xenon Boostrap Admin Panel" />
	<meta name="author" content="" />

	<link rel="icon" href="<?=base_url()?>assets/images/icon.png">

	<title>ARO - Ads Radio Online </title>

	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:400,700,400italic">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/fonts/linecons/css/linecons.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/fonts/fontawesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/xenon-core.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/xenon-forms.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/xenon-components.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/xenon-skins.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/custom.css">
	<script src="<?=base_url()?>assets/js/jquery-1.11.1.min.js"></script>

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->


</head>
<body class="page-body">

	<div class="settings-pane">
			
		<a href="#" data-toggle="settings-pane" data-animate="true">
			&times;
		</a>
		
		<div class="settings-pane-inner">
			
			<div class="row">
				
				<div class="col-md-4">
					
					<div class="user-info">
						
						<div class="user-image">
							<a href="#">
								<img src="<?=base_url()?>assets/usuarios/<?=$session_data['username']?>.jpg" class="img-responsive img-circle" />
							</a>
						</div>
						
						<div class="user-details">
							
							<h3>
								<a href="#"><?=$session_data['username']?></a>
								
								<!-- Available statuses: is-online, is-idle, is-busy and is-offline -->
								<span class="user-status is-online"></span>
							</h3>
							
							<p class="user-title">Web Developer</p>
							
							<div class="user-links">
								<a href="#" class="btn btn-primary">Edit Profile</a>
								<a href="#" class="btn btn-success">Upgrade</a>
							</div>
							
						</div>
						
					</div>
					
				</div>
				
				<div class="col-md-8 link-blocks-env">
					
					<div class="links-block left-sep">
						<h4>
							<span>Notifications</span>
						</h4>
						
						<ul class="list-unstyled">
							<li>
								<input type="checkbox" class="cbr cbr-primary" checked="checked" id="sp-chk1" />
								<label for="sp-chk1">Messages</label>
							</li>
							<li>
								<input type="checkbox" class="cbr cbr-primary" checked="checked" id="sp-chk2" />
								<label for="sp-chk2">Events</label>
							</li>
							<li>
								<input type="checkbox" class="cbr cbr-primary" checked="checked" id="sp-chk3" />
								<label for="sp-chk3">Updates</label>
							</li>
							<li>
								<input type="checkbox" class="cbr cbr-primary" checked="checked" id="sp-chk4" />
								<label for="sp-chk4">Server Uptime</label>
							</li>

							
						</ul>
					</div>
					
					<div class="links-block left-sep">
						<h4>
							<a href="#">
								<span>Help Desk</span>
							</a>
						</h4>
						
						<ul class="list-unstyled">
							<li>
								<a href="#">
									<i class="fa-angle-right"></i>
									Support Center
								</a>
							</li>
							<li>
								<a href="#">
									<i class="fa-angle-right"></i>
									Submit a Ticket
								</a>
							</li>
							<li>
								<a href="#">
									<i class="fa-angle-right"></i>
									Domains Protocol
								</a>
							</li>
							<li>
								<a href="#">
									<i class="fa-angle-right"></i>
									Terms of Service
								</a>
							</li>
						</ul>
					</div>
					
				</div>
				
			</div>
		
		</div>
		
	</div>
	
	<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
			
		<!-- Add "fixed" class to make the sidebar fixed always to the browser viewport. -->
		<!-- Adding class "toggle-others" will keep only one menu item open at a time. -->
		<!-- Adding class "collapsed" collapse sidebar root elements and show only icons. -->
		<div class="sidebar-menu toggle-others fixed">
		
			<div class="sidebar-menu-inner">
				
				<header class="logo-env">
		
					<!-- logo -->
					<div class="logo">
						<a href="<?php echo base_url();?>" class="logo-expanded">
							<img src="<?=base_url()?>assets/images/logo@2x.png" width="100" alt="" />
						</a>
		
						<a href="<?php echo base_url();?>" class="logo-collapsed">
							<img src="<?=base_url()?>assets/images/logo-collapsed@2x.png" width="50" alt="" />
						</a>
					</div>
		
					<!-- This will toggle the mobile menu and will be visible only on mobile devices -->
					<div class="mobile-menu-toggle visible-xs">
						<a href="#" data-toggle="user-info-menu">
							<i class="fa-bell-o"></i>
							<span class="badge badge-success">7</span>
						</a>
		
						<a href="#" data-toggle="mobile-menu">
							<i class="fa-bars"></i>
						</a>
					</div>
		
					<!-- This will open the popup with user profile settings, you can use for any purpose, just be creative -->
					<div class="settings-icon">
						<a href="#" data-toggle="settings-pane" data-animate="true">
							<i class="linecons-cog"></i>
						</a>
					</div>
		
					
				</header>



				<!-- Sidebar User Info Bar - Added in 1.3 -->
				<section class="sidebar-user-info" >
					<div class="sidebar-user-info-inner">
						<a href="#" class="user-profile">
							<img src="<?=base_url()?>assets/usuarios/<?=$session_data['username']?>.jpg" width="60" height="60" class="img-circle img-corona" alt="user-pic" />
				
							<span>
								<strong><?=$session_data['username']?></strong>
								Administrador
							</span>
						</a>
				
						
					</div>
				</section>
				
				
						
				<ul id="main-menu" class="main-menu">
							<!-- add class "multiple-expanded" to allow multiple submenus to open -->
					<!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
					
					<li>
						<a href="<?=base_url()?>enrutador">
							<i class="fa-rocket"></i>
							<span class="title"> Enrutador</span>
						</a>
						
					</li>
					
					<li>
						<a href="<?=base_url()?>ads/prerolls">
							<i class="fa-bullhorn"></i>
							<span class="title">Ads</span>
						</a>
						<ul>
							<li>

								<a href="<?=base_url()?>ads/prerolls">
								<i class="fa-bullseye"></i>
									<span class="title">Pre-rolls</span>
								</a>
							</li>
						</ul>
					</li>


					<li>
						<a href="<?=base_url()?>bucket">
							<i class="fa-cloud"></i>
							<span class="title">S3 Bucket</span>
						</a>
						<ul>
							
							<li>
								<a href="<?=base_url()?>bucket">
								<i class="fa-file-audio-o"></i>
									<span class="title">S3 Bucket</span>
								</a>
							</li>
					

							
						</ul>
					</li>


							<li>
						<a href="<?=base_url()?>home/cachecontrol">
							<i class="fa-rocket"></i>
							<span class="title"> Cache Control</span>
						</a>
						
					</li>
					

					<li>
						<a href="dashboard-1.html">
							<i class="fa-gears"></i>
							<span class="title"> Manage System</span>
						</a>
						<ul>
							<li>
								<a href="<?=base_url()?>ads/usuarios">
								<i class="fa-users"></i>
									<span class="title">Usuarios</span>
								</a>
							</li>

							
							<li>
								<a href="<?=base_url()?>ads/prerolls">
								
								<i class="fa-bullseye"></i>
									<span class="title">Pre-roll</span>
								</a>
							</li>

							<li>
								<a href="<?=base_url()?>ads/enrutadores">
								<i class="fa-rocket"></i>
									<span class="title">Enrutadores</span>
								</a>
							</li>

							<li>
								<a href="<?=base_url()?>ads/radios">
								<i class="fa-signal"></i>
									<span class="title">Radios</span>
								</a>
							</li>	
							<li>
								<a href="<?=base_url()?>ads/estados">
								<i class="fa-circle"></i>
									<span class="title">Estados</span>
								</a>
							</li>



							
						</ul>
					</li>

					
					
					
					
				</ul>



		
				
			</div>



		</div>
	
		<div class="main-content">
					
			<nav class="navbar user-info-navbar"  role="navigation"><!-- User Info, Notifications and Menu Bar -->
			
				
			
				<!-- Right links for user info navbar -->
				<ul class="user-info-menu right-links list-inline list-unstyled">
							<li class="search-form"><!-- You can add "always-visible" to show make the search input visible -->
			
						<form name="userinfo_search_form" method="get" action="extra-search.html">
							<input type="text" name="s" class="form-control search-field" placeholder="Type to search..." />
			
							<button type="submit" class="btn btn-link">
								<i class="linecons-search"></i>
							</button>
						</form>
			
					</li>
			
					<li class="dropdown user-profile">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<img src="<?=base_url()?>assets/usuarios/<?=$session_data['username']?>.jpg" alt="user-image" class="img-circle img-inline userpic-32" width="28" />
							<span>
								<?=$session_data['username']?>
								<i class="fa-angle-down"></i>
							</span>
						</a>
			
						<ul class="dropdown-menu user-profile-menu list-unstyled">
							<li>
								<a href="<?=base_url()?>enrutador">
									<i class="fa-edit"></i>
									Actualizar Enrutador
								</a>
							</li>

							<li>
								<a href="<?=base_url()?>ads/prerolls/add">
									<i class="fa-edit"></i>
									Nuevo Ad Preroll
								</a>
							</li>
							<li>
								<a href="#settings">
									<i class="fa-wrench"></i>
									Settings
								</a>
							</li>
							<li>
								<a href="#profile">
									<i class="fa-user"></i>
									Profile
								</a>
							</li>
							<li>
								<a href="#help">
									<i class="fa-info"></i>
									Help
								</a>
							</li>
							<li class="last">
								<a href="<?=base_url()?>home/logout/">
									<i class="fa-lock"></i>
									Logout
								</a>
							</li>
						</ul>
					</li>
			
					<li>
						<a href="#" data-toggle="chat">
							<i class="fa-comments-o"></i>
						</a>
					</li>
			
				</ul>
			
			</nav>
			




