



			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->
			<footer class="main-footer sticky footer-type-1">
				
				<div class="footer-inner">
				
					<!-- Add your copyright text here -->
					<div class="footer-text">
						&copy; 2016 
						<strong>ARO Ads Radio Online - Admin</strong> 
						
					</div>
					
					
					<!-- Go to Top Link, just add rel="go-top" to any link to add this functionality -->
					<div class="go-up">
					
						<a href="#" rel="go-top">
							<i class="fa-angle-up"></i>
						</a>
						
					</div>
					
				</div>
				
			</footer>
		</div>
	
		
	
	</div>
	
	



	<!-- Bottom Scripts -->
	<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
	<script src="<?=base_url()?>assets/js/TweenMax.min.js"></script>
	<script src="<?=base_url()?>assets/js/resizeable.js"></script>
	<script src="<?=base_url()?>assets/js/joinable.js"></script>
	<script src="<?=base_url()?>assets/js/xenon-api.js"></script>
	<script src="<?=base_url()?>assets/js/xenon-toggles.js"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="<?=base_url()?>assets/js/xenon-custom.js"></script>

</body>
</html>