<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<h1><?=$session_data['nombres']?>, Bienvenido a ARO Ads Radio Online</h1>


<!-- #######  YAY, I AM THE SOURCE EDITOR! #########-->
<div><strong><br />ARO Admin 1.0&nbsp;</strong>: Es un sistema para la gesti&oacute;n&nbsp;de&nbsp;Ads Preroll&nbsp;y el origen de la plataforma que sirve el Ad Prerolls .&nbsp;</div>
<div>&nbsp;</div>
<div>
<ol>
<li><strong>ENRUTADOR</strong>: Indicar al Player que fuente de Ad debe de transmitir por Radio.<br /><br /></li>
<ol>
<li><strong>Ninguno</strong>: Indica al Player que no tiene relacionado ning&uacute;n Ad preroll por tanto reproduce la transmisi&oacute;n normal.<br /><br /></li>
<li><strong>Aro&nbsp;</strong>: Indica al&nbsp;Player&nbsp;que existe un preroll (Audio / Video ) Activo.<br /><br /></li>
<li><strong>Tap</strong>: Indica al Player que existe &nbsp;un preroll (Audio / Video ) Activo que carga directamente de la linea de Streaming de Triton&nbsp;<br /><br /></li>
</ol>
<li><strong>ADS / PREROLLS: &nbsp;</strong>Gestiona las &nbsp;Ads (audios y videos pre-roll) relacionadas a cada radio.<br /><br /></li>
<li><strong>S3 Bucket :&nbsp;</strong>Permite listar el contenidos del S3 bucket &nbsp;de Amazon (ads.crp.com.pe) , aqu&iacute; podemos validar que el audio o video exista en el Bucket<br /><br /><br /></li>
<li>&nbsp;<strong>MANAGE SYSTEM:</strong> &nbsp;Es el core del Sistema permite gestionar los siguientes m&oacute;dulos. Solo los usuarios administradores tienen acceso.</li>
</ol>
<ol>
<ol>
<li>Usuarios</li>
<li>Prerolls</li>
<li>Enrutador</li>
<li>Radios</li>
<li>Estados&nbsp;</li>
</ol>
</ol>
</div>