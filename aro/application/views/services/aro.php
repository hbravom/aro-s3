<?php

/**
 * 
 * $preroll['tipo'] distingue el tipo de preroll (audio /video)
 * 
 */

switch ($preroll['tipo']) {

	case 1: //Caso audio, mostramos el reproductor de audio de html5 
	        //El javascript valida el evento Ended del Audio para luego reproducir el Streaming con nomalidad en modo ARO.

			echo '
			    	
			    <audio autoplay id="AudioPreroll" >
			        <source src="'.$preroll['path_file'].'" type="audio/mp3">
			        Tu Browser no soporta Html5 Video. <a href="" onclick=" VideoEnd();"> Continuar</a>
			    </audio>

			    <script>

			        //document.getElementById("code_lead").style.display = "block";
			        //document.getElementById("code_bigbox").style.display = "block";

			        pre_roll=1;			       
			        var AudioPreroll = document.getElementById("AudioPreroll");
					
					 AudioPreroll.addEventListener("play", function () {
							 ga(\'send\', \'event\', \' tracking Streaming\',\'Play Preroll\');
					 });

			        AudioPreroll.addEventListener("ended", function () {	     		
				       	             ga(\'send\', \'event\', \' tracking Streaming\',\'End Preroll\');
			        			   console.log(\'Evento Analitycs Ok\');

				          if(mobile == 0) //DESKTOP
						    {
						    	ReadyStreaming();
						    	ShowBtnPause();						          
						      
						      $( "#pauseButton" ).attr( "onclick", "  ShowBtnPlay(); pauseStream();" );
	      				   $( "#playButton" ).attr( "onclick", "ShowBtnPause(); playStream();" );      				                     
						    	
						    }
						    else  if(mobile == 1) //MOBILE
						    {						    	  
						    	  //playStream();
						    	  //ShowBtnPauseMobile();
						            $( "#pauseButton" ).attr( "onclick", "ShowBtnPlayMobile(); pauseStream();");           
						            $( "#playMobile" ).attr( "onclick", "ShowBtnPauseMobile(); playStream();" );

						           // $("#playMobile").click();            				       
						         
						    }    

						     // Flag escucho
			        			      flag_escucho=true;
			        			      console.log("Se escuchó por primera vez: var flag_escucho ="+ flag_escucho);
			        });

			            AudioPreroll.addEventListener("error", function () {
				            console.log("Preroll: error de carga de cuña");
				            ended = 1; //variable indica video acabó
				            ReadyStreaming();  //iniciar el Streaming con Segid

			        });


			        //En caso de Mobile el Audio Será reproducido por primera vez con el PlayButton 

			        if(mobile == 1){			        	 

						ShowBtnPlayMobile();
						$("#modalAro").modal("show");	

						$("#playMobile").attr( "onclick", "AudioPreroll.play();" );
						//$("#playMobile").click();
	        	
    		    	}



			    </script>
			   ';	
			   
			 

			  
		break;

	case 2: //video

		//caso Video , muestra el reproductor de Video Html5 
		// El javascript valida el evento Ended del Video para luego reproducir el Streaming con nomalidad en modo ARO.
		
		echo '
				<script>
					var video = $("<video />", {
						id: "videopreroll",
						src: "'.$preroll['path_file'].'",
						type: "video/mp4",
						controls: false,
						autoplay: true,
						height: 350
					});
					video.appendTo($("#div_video"));
					var aud = document.getElementById("videopreroll");
				
				console.log("mobile: "+mobile);

				if(mobile == 1)
				{	
					aud.controls = true;
					aud.width = 320; 
					aud.play();
				}
				
				
				 aud.addEventListener("play", function () {
					ga(\'send\', \'event\', \' tracking Streaming\',\'Play Video Preroll\');
					console.log("Video Preroll - Play");
				 });

			    aud.addEventListener("ended", function () {	     		
					ga(\'send\', \'event\', \' tracking Streaming\',\'End Video Preroll\');
					console.log(\'Evento Analitycs Ok\');
					console.log("Video Preroll - End");
				 });


				aud.onended = function() {
				  
				    $("#div_video").hide();

				    		if(mobile == 0) //DESKTOP
						    {
						    	ReadyStreaming();
						    	ShowBtnPause();						          
						      
						      $( "#pauseButton" ).attr( "onclick", "  ShowBtnPlay(); pauseStream();" );
	      				   $( "#playButton" ).attr( "onclick", "ShowBtnPause(); playStream();" );      				                     
						    	
						    }
						    else  if(mobile == 1) //MOBILE
						    {						    	  

						    	  ShowBtnPlayMobile();
						            $( "#pauseButton" ).attr( "onclick", "ShowBtnPlayMobile(); pauseStream();");           
						            $( "#playMobile" ).attr( "onclick", "ShowBtnPauseMobile(); playStream();" );

						                   				       
						         
						    }    
					 
					 ended = 1;
				};
				
				console.log("RADIO: '.$radio_id.'");
				</script>';
		break;
	
	
}



?>