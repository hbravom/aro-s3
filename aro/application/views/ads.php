
<div class="page-title">

	<div class="title-env">
		<h1 class="title">Manage System</h1>
		<p class="description">(Administración del sistema)</p>
	</div>

	<div class="breadcrumb-env">

		<ol class="breadcrumb bc-1" >
			<li>
				<a href="<?php echo base_url();?>"><i class="fa-home"></i>Home</a>
			</li>
			<li>
				<a href="<?php echo base_url();?>"></i>Manage</a>
			</li>
			
			<li class="active">

				<strong>System	</strong>
			</li>
		</ol>

	</div>

</div>




<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<style type='text/css'>
body
{
	font-family: Arial;
	font-size: 14px;
}
a {
    color: blue;
    text-decoration: none;
    font-size: 14px;
}
a:hover
{
	text-decoration: underline;
}
</style>
</head>
<body>
	<div>
		<a href='<?php echo site_url('ads/usuarios')?>'>Usuarios</a> |
		<a href='<?php echo site_url('ads/prerolls')?>'>Preroll</a> |
		<a href='<?php echo site_url('ads/enrutadores')?>'>Enrutador</a> |
		<a href='<?php echo site_url('ads/radios')?>'>Radios</a> |
		<a href='<?php echo site_url('ads/estados')?>'>Estados</a> |
		
		
		
	</div>
	<div style='height:20px;'></div>  
    <div>
		<?php echo $output; ?>
    </div>
</body>
</html>
