


<div class="page-title">

	<div class="title-env">
		<h1 class="title">Enrutador</h1>
		<p class="description"> Indicadores de origen de los Ads por radios </p>
	</div>

	<div class="breadcrumb-env">

		<ol class="breadcrumb bc-1" >
			<li>
				<a href="<?php echo base_url();?>"><i class="fa-home"></i>Home</a>
			</li>
			
			<li class="active">

				<strong>Enrutador</strong>
			</li>
		</ol>

	</div>

</div>








<!-- Responsive Table -->
<div class="row">
	<div class="col-md-12">

		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Enrutadores por radio</h3>

				<div class="panel-options">
					<a href="#">
						<i class="linecons-cog"></i>
					</a>

					<a href="#" data-toggle="panel">
						<span class="collapse-icon">&ndash;</span>
						<span class="expand-icon">+</span>
					</a>

					<a href="#" data-toggle="reload">
						<i class="fa-rotate-right"></i>
					</a>

					<a href="#" data-toggle="remove">
						&times;
					</a>
				</div>
			</div>




			

			<div class="panel-body">

						
				

				
				<div class="table-responsive" data-pattern="priority-columns" data-focus-btn-icon="fa-asterisk" data-sticky-table-header="true" data-add-display-all-btn="true" data-add-focus-btn="true">

					<table cellspacing="0" class="table table-small-font table-bordered table-striped">
						<thead>
							<tr>
								<th>Radio</th>
								<th>Ninguno</th>
								<th>ARO</th>
								<th>TAP</th>
							</tr>
						</thead>
						<tbody>
						<?php						
							
							
							$list='';	
							$rand=rand(2,100);
							$string = file_get_contents(base_url()."assets/enrutador.json?cache=".$rand);
							$enrutador = json_decode($string, true);
								
														 
							for ($i=0; $i< count($enrutador); $i++){							
							 
							    $radio = $enrutador[$i]['radio'];
							    $opcion= $enrutador[$i]['opcion'];
							     
							     //colores
							    $color_check='success';
							    //Textos
							    $checkbox_ninguno='';
							    $checkbox_aro='';
							    $checkbox_tap='';	

							    switch ($opcion) {
							    	case 1:
							    		$checkbox_ninguno=' checked ';
							    		break;
							    	case 2:
							    		$checkbox_aro=' checked ';
							    		break;
							    	case 3:
							    		$checkbox_tap=' checked ';
							    		break;
							    	
							    	default:
							    		$checkbox_ninguno=' checked ';
							    		break;
							    		
							    }
							    

							   

							    $button_ninguno='<div class="form-block">
									<input type="radio" class="iswitch-xlg iswitch-'.$color_check.'" '.$checkbox_ninguno.' onclick="UpdateEnrutador('.$i.',\'1\');" >
									</div>';

							    $button_aro='<div class="form-block">
									<input type="radio" class="iswitch-xlg iswitch-'.$color_check.'" '.$checkbox_aro.'  onclick="UpdateEnrutador('.$i.',\'2\');" >
									</div>';

							    $button_tap='<div class="form-block">
									<input type="radio" class="iswitch-xlg iswitch-'.$color_check.'" '.$checkbox_tap.'   onclick="UpdateEnrutador('.$i.',\'3\');" >
									</div>';


							  
							    $list.='<tr>
								<th>'.$radio.'</th>
								<td>'.$button_ninguno.'</td>
								<td>'.$button_aro.'</td>
								<td>'.$button_tap.'</td>

								
								
								</tr>';
							}
							echo $list;


						?>




							<?php 

							
						?>
						<script type="text/javascript">							
							  
							   function UpdateEnrutador(radio, opcion){

							   	setTimeout(function(){ window.location="<?=base_url()?>enrutador/update/"+radio+"/"+opcion; }, 1000);
							   	
							   }						

						</script>

					</tbody>
				</table>

			</div>




<!--	